const request = require('request-promise');
const cheerio = require("cheerio");

module.exports = {
    get: async (stock) => {
        let response = await request(`https://statusinvest.com.br/acoes/${stock}`);

        let $ = cheerio.load(response);

        let preco = await $('div[title="Valor atual do ativo"] > strong').text()
        let variacao = await $('span[title="Variação do valor do ativo com base no dia anterior"] > b').text()
        return `Ação: ${stock}\nPreço: ${preco}\nVariação: ${variacao}`;
    }
}