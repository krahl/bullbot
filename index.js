const token = "<SEU TOKEN AQUI>";
const Telegraf = require('telegraf');
const bot = new Telegraf(token);
const stock = require("./stock")

bot.start( async ctx => {
    const name = ctx.update.message.from.first_name
    await ctx.reply(`Seja bem vindo, ${name}`)
    await ctx.reply("Me manda códigos de ações, que lhe mostro o valor...")
    ctx.replyWithVideo({url: `https://media.giphy.com/media/YnkMcHgNIMW4Yfmjxr/giphy.mp4`})
})

bot.on('text', ctx => {
    let texto = ctx.update.message.text
    stock.get(texto.toUpperCase()).then( (msg) => {
        ctx.reply(msg);
    }); 
});

bot.launch();